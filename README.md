# Information / Информация

SPEC-файл для создания RPM-пакета **icecast**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/icecast`.
2. Установить пакет: `dnf install icecast`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)